package com.sridher.firstdemo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomService {
	@Autowired
private RoommatesRepository Repo;
	
	
	public List<Roommates> listAll(){
		return Repo.findAll();
	}
	public Roommates save(Roommates roommates) {
		//Repo.save(roommates);
		return Repo.save(roommates);
	}
	public Roommates get(int id) {
		return Repo.findById(id).get();
		
	}
    public void delete(int id) {
	       Repo.deleteById(id);

}


}
