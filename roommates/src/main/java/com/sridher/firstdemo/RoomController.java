package com.sridher.firstdemo;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomController {
	 @Autowired 
	 private RoomService roomservice;
	  @GetMapping("/all")
	  @ResponseBody 
	  public List<Roommates> getAll() {
		  return roomservice.listAll();
	  }
	  @GetMapping("/{id}")
	 // @ResponseBody
	  public Roommates getroommate(@PathVariable int id) {
		return roomservice.get(id);
	  }
	  @PostMapping
	
	  public Roommates create(@Valid @RequestBody Roommates roommates) {
		  return roomservice.save(roommates);
	  }
	 
	 
}
