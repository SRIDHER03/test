package com.sridher.firstdemo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoommatesRepository extends JpaRepository<Roommates, Integer > {

}
